<?php

/* /var/www/atelier/themes/jtherczeg-holidaycrown/partials/meta.htm */
class __TwigTemplate_ff9ac86f890ab8391c8ed12a627a903c579aef1973abca29a1cb92af6c841445 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<head>
<meta charset=\"utf-8\">
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
<meta name=\"description\" content=\"";
        // line 4
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "meta_description", array()), "html", null, true);
        echo "\">
<meta name=\"keywords\" content=\"";
        // line 5
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "meta_keywords", array()), "html", null, true);
        echo "\">
<meta name=\"author\" content=\"";
        // line 6
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "meta_author", array()), "html", null, true);
        echo "\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\">
<title>";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "theme", array()), "site_name", array()), "html", null, true);
        echo " | ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "page", array()), "title", array()), "html", null, true);
        echo "</title>

<!-- Google fonts -->
<link href='http://fonts.googleapis.com/css?family=Raleway:300,500,800|Old+Standard+TT' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:300,500,800' rel='stylesheet' type='text/css'>
<!-- font awesome -->
<link href=\"//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css\" rel=\"stylesheet\">
<!-- bootstrap -->
<link href=\"";
        // line 16
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter(array(0 => "assets/vendor/bootstrap/css/bootstrap.min.css", 1 => "assets/vendor/uniform/css/uniform.default.min.css", 2 => "assets/vendor/wow/animate.css", 3 => "assets/vendor/gallery/blueimp-gallery.min.css", 4 => "assets/css/style.css"));
        // line 22
        echo "\" rel=\"stylesheet\">
<!-- favicon -->
<link rel=\"shortcut icon\" href=\"";
        // line 24
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/favicon.png");
        echo "\" type=\"image/x-icon\">
<link rel=\"icon\" href=\"";
        // line 25
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/favicon.png");
        echo "\" type=\"image/x-icon\">

</head>";
    }

    public function getTemplateName()
    {
        return "/var/www/atelier/themes/jtherczeg-holidaycrown/partials/meta.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 25,  56 => 24,  52 => 22,  50 => 16,  37 => 8,  32 => 6,  28 => 5,  24 => 4,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<head>
<meta charset=\"utf-8\">
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />
<meta name=\"description\" content=\"{{ this.page.meta_description }}\">
<meta name=\"keywords\" content=\"{{ this.page.meta_keywords }}\">
<meta name=\"author\" content=\"{{ this.page.meta_author }}\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no\">
<title>{{ this.theme.site_name }} | {{ this.page.title }}</title>

<!-- Google fonts -->
<link href='http://fonts.googleapis.com/css?family=Raleway:300,500,800|Old+Standard+TT' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:300,500,800' rel='stylesheet' type='text/css'>
<!-- font awesome -->
<link href=\"//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css\" rel=\"stylesheet\">
<!-- bootstrap -->
<link href=\"{{ [
            'assets/vendor/bootstrap/css/bootstrap.min.css',
            'assets/vendor/uniform/css/uniform.default.min.css',
            'assets/vendor/wow/animate.css',
            'assets/vendor/gallery/blueimp-gallery.min.css',
            'assets/css/style.css',
        ]|theme }}\" rel=\"stylesheet\">
<!-- favicon -->
<link rel=\"shortcut icon\" href=\"{{ 'assets/images/favicon.png'|theme }}\" type=\"image/x-icon\">
<link rel=\"icon\" href=\"{{ 'assets/images/favicon.png'|theme }}\" type=\"image/x-icon\">

</head>", "/var/www/atelier/themes/jtherczeg-holidaycrown/partials/meta.htm", "");
    }
}
