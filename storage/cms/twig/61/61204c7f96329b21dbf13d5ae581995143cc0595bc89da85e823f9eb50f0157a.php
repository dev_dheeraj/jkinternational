<?php

/* /var/www/atelier/themes/jtherczeg-holidaycrown/partials/banner.htm */
class __TwigTemplate_cb8d9fe96b5c66bdd1717a86368d2e64bebb2896c22859a2219d707817222295 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<img src=\"";
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/photos/banner.jpg");
        echo "\"  class=\"img-responsive\" alt=\"slide\">
    <div class=\"welcome-message\">
        <div class=\"wrap-info\">
            <div class=\"information\">
                <h1  class=\"animated fadeInDown\">Best Hotel in Himachal</h1>
                <p class=\"animated fadeInUp\">Most luxurious hotel of Asia with the royal treatments and excellent customer service.</p>                
            </div>
            <a href=\"#information\" class=\"arrow-nav scroll wowload fadeInDownBig\"><i class=\"fa fa-angle-down\"></i></a>
        </div>
    </div>";
    }

    public function getTemplateName()
    {
        return "/var/www/atelier/themes/jtherczeg-holidaycrown/partials/banner.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<img src=\"{{ 'assets/images/photos/banner.jpg'|theme }}\"  class=\"img-responsive\" alt=\"slide\">
    <div class=\"welcome-message\">
        <div class=\"wrap-info\">
            <div class=\"information\">
                <h1  class=\"animated fadeInDown\">Best Hotel in Himachal</h1>
                <p class=\"animated fadeInUp\">Most luxurious hotel of Asia with the royal treatments and excellent customer service.</p>                
            </div>
            <a href=\"#information\" class=\"arrow-nav scroll wowload fadeInDownBig\"><i class=\"fa fa-angle-down\"></i></a>
        </div>
    </div>", "/var/www/atelier/themes/jtherczeg-holidaycrown/partials/banner.htm", "");
    }
}
