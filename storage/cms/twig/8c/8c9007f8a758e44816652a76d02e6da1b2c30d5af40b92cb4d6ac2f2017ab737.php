<?php

/* /var/www/atelier/themes/jtherczeg-holidaycrown/pages/samples/gallery.htm */
class __TwigTemplate_49a5e6380e688e1d79b77bf4f568f1996f5cf5d8e85694167bfdd4037e303c38 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"container\">

       <h1 class=\"title\">Gallery</h1>
       <div class=\"row gallery\">
              <div class=\"col-sm-4 wowload fadeInUp\"><a href=\"";
        // line 5
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/photos/1.jpg");
        echo "\" title=\"Foods\" class=\"gallery-image\" data-gallery><img src=\"";
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/photos/1.jpg");
        echo "\" class=\"img-responsive\"></a></div>
              <div class=\"col-sm-4 wowload fadeInUp\"><a href=\"";
        // line 6
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/photos/2.jpg");
        echo "\" title=\"Coffee\" class=\"gallery-image\" data-gallery><img src=\"";
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/photos/2.jpg");
        echo "\" class=\"img-responsive\"></a></div>
              <div class=\"col-sm-4 wowload fadeInUp\"><a href=\"";
        // line 7
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/photos/3.jpg");
        echo "\" title=\"Travel\" class=\"gallery-image\" data-gallery><img src=\"";
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/photos/3.jpg");
        echo "\" class=\"img-responsive\"></a></div>
              <div class=\"col-sm-4 wowload fadeInUp\"><a href=\"";
        // line 8
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/photos/4.jpg");
        echo "\" title=\"Adventure\" class=\"gallery-image\" data-gallery><img src=\"";
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/photos/4.jpg");
        echo "\" class=\"img-responsive\"></a></div>
              <div class=\"col-sm-4 wowload fadeInUp\"><a href=\"";
        // line 9
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/photos/5.jpg");
        echo "\" title=\"Fruits\" class=\"gallery-image\" data-gallery><img src=\"";
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/photos/5.jpg");
        echo "\" class=\"img-responsive\"></a></div>
              <div class=\"col-sm-4 wowload fadeInUp\"><a href=\"";
        // line 10
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/photos/6.jpg");
        echo "\" title=\"Summer\" class=\"gallery-image\" data-gallery><img src=\"";
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/photos/6.jpg");
        echo "\" class=\"img-responsive\"></a></div>
              <div class=\"col-sm-4 wowload fadeInUp\"><a href=\"";
        // line 11
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/photos/7.jpg");
        echo "\" title=\"Bathroom\" class=\"gallery-image\" data-gallery><img src=\"";
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/photos/7.jpg");
        echo "\" class=\"img-responsive\"></a></div>
              <div class=\"col-sm-4 wowload fadeInUp\"><a href=\"";
        // line 12
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/photos/8.jpg");
        echo "\" title=\"Rooms\" class=\"gallery-image\" data-gallery><img src=\"";
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/photos/8.jpg");
        echo "\" class=\"img-responsive\"></a></div>
              <div class=\"col-sm-4 wowload fadeInUp\"><a href=\"";
        // line 13
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/photos/9.jpg");
        echo "\" title=\"Big Room\" class=\"gallery-image\" data-gallery><img src=\"";
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/photos/9.jpg");
        echo "\" class=\"img-responsive\"></a></div>
              <div class=\"col-sm-4 wowload fadeInUp\"><a href=\"";
        // line 14
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/photos/11.jpg");
        echo "\" title=\"Living Room\" class=\"gallery-image\" data-gallery><img src=\"";
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/photos/11.jpg");
        echo "\" class=\"img-responsive\"></a></div>
              <div class=\"col-sm-4 wowload fadeInUp\"><a href=\"";
        // line 15
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/photos/1.jpg");
        echo "\" title=\"Fruits\" class=\"gallery-image\" data-gallery><img src=\"";
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/photos/1.jpg");
        echo "\" class=\"img-responsive\"></a></div>
              <div class=\"col-sm-4 wowload fadeInUp\"><a href=\"";
        // line 16
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/photos/3.jpg");
        echo "\" title=\"Travel\" class=\"gallery-image\" data-gallery><img src=\"";
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/photos/3.jpg");
        echo "\" class=\"img-responsive\"></a></div>
       </div>
</div>";
    }

    public function getTemplateName()
    {
        return "/var/www/atelier/themes/jtherczeg-holidaycrown/pages/samples/gallery.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 16,  85 => 15,  79 => 14,  73 => 13,  67 => 12,  61 => 11,  55 => 10,  49 => 9,  43 => 8,  37 => 7,  31 => 6,  25 => 5,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"container\">

       <h1 class=\"title\">Gallery</h1>
       <div class=\"row gallery\">
              <div class=\"col-sm-4 wowload fadeInUp\"><a href=\"{{ 'assets/images/photos/1.jpg'|theme }}\" title=\"Foods\" class=\"gallery-image\" data-gallery><img src=\"{{ 'assets/images/photos/1.jpg'|theme }}\" class=\"img-responsive\"></a></div>
              <div class=\"col-sm-4 wowload fadeInUp\"><a href=\"{{ 'assets/images/photos/2.jpg'|theme }}\" title=\"Coffee\" class=\"gallery-image\" data-gallery><img src=\"{{ 'assets/images/photos/2.jpg'|theme }}\" class=\"img-responsive\"></a></div>
              <div class=\"col-sm-4 wowload fadeInUp\"><a href=\"{{ 'assets/images/photos/3.jpg'|theme }}\" title=\"Travel\" class=\"gallery-image\" data-gallery><img src=\"{{ 'assets/images/photos/3.jpg'|theme }}\" class=\"img-responsive\"></a></div>
              <div class=\"col-sm-4 wowload fadeInUp\"><a href=\"{{ 'assets/images/photos/4.jpg'|theme }}\" title=\"Adventure\" class=\"gallery-image\" data-gallery><img src=\"{{ 'assets/images/photos/4.jpg'|theme }}\" class=\"img-responsive\"></a></div>
              <div class=\"col-sm-4 wowload fadeInUp\"><a href=\"{{ 'assets/images/photos/5.jpg'|theme }}\" title=\"Fruits\" class=\"gallery-image\" data-gallery><img src=\"{{ 'assets/images/photos/5.jpg'|theme }}\" class=\"img-responsive\"></a></div>
              <div class=\"col-sm-4 wowload fadeInUp\"><a href=\"{{ 'assets/images/photos/6.jpg'|theme }}\" title=\"Summer\" class=\"gallery-image\" data-gallery><img src=\"{{ 'assets/images/photos/6.jpg'|theme }}\" class=\"img-responsive\"></a></div>
              <div class=\"col-sm-4 wowload fadeInUp\"><a href=\"{{ 'assets/images/photos/7.jpg'|theme }}\" title=\"Bathroom\" class=\"gallery-image\" data-gallery><img src=\"{{ 'assets/images/photos/7.jpg'|theme }}\" class=\"img-responsive\"></a></div>
              <div class=\"col-sm-4 wowload fadeInUp\"><a href=\"{{ 'assets/images/photos/8.jpg'|theme }}\" title=\"Rooms\" class=\"gallery-image\" data-gallery><img src=\"{{ 'assets/images/photos/8.jpg'|theme }}\" class=\"img-responsive\"></a></div>
              <div class=\"col-sm-4 wowload fadeInUp\"><a href=\"{{ 'assets/images/photos/9.jpg'|theme }}\" title=\"Big Room\" class=\"gallery-image\" data-gallery><img src=\"{{ 'assets/images/photos/9.jpg'|theme }}\" class=\"img-responsive\"></a></div>
              <div class=\"col-sm-4 wowload fadeInUp\"><a href=\"{{ 'assets/images/photos/11.jpg'|theme }}\" title=\"Living Room\" class=\"gallery-image\" data-gallery><img src=\"{{ 'assets/images/photos/11.jpg'|theme }}\" class=\"img-responsive\"></a></div>
              <div class=\"col-sm-4 wowload fadeInUp\"><a href=\"{{ 'assets/images/photos/1.jpg'|theme }}\" title=\"Fruits\" class=\"gallery-image\" data-gallery><img src=\"{{ 'assets/images/photos/1.jpg'|theme }}\" class=\"img-responsive\"></a></div>
              <div class=\"col-sm-4 wowload fadeInUp\"><a href=\"{{ 'assets/images/photos/3.jpg'|theme }}\" title=\"Travel\" class=\"gallery-image\" data-gallery><img src=\"{{ 'assets/images/photos/3.jpg'|theme }}\" class=\"img-responsive\"></a></div>
       </div>
</div>", "/var/www/atelier/themes/jtherczeg-holidaycrown/pages/samples/gallery.htm", "");
    }
}
