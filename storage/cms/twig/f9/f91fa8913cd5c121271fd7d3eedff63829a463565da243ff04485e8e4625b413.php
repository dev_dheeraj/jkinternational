<?php

/* /var/www/atelier/themes/vojtasvoboda-newage/partials/features/dummy.htm */
class __TwigTemplate_9cf5d3b617d37f9e8bd0a61571bddf0f4f4666d0f601d9ab8751a4f5a4371d63 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"row\">
    <div class=\"col-lg-6\">
        <div class=\"feature-item\">
            <i class=\"icon-screen-smartphone text-primary\"></i>
            <h3>Device Mockups</h3>
            <p class=\"text-muted\">Ready to use HTML/CSS device mockups, no Photoshop required!</p>
        </div>
    </div>
    <div class=\"col-lg-6\">
        <div class=\"feature-item\">
            <i class=\"icon-camera text-primary\"></i>
            <h3>Flexible Use</h3>
            <p class=\"text-muted\">Put an image, video, animation, or anything else in the screen!</p>
        </div>
    </div>
</div>
<div class=\"row\">
    <div class=\"col-lg-6\">
        <div class=\"feature-item\">
            <i class=\"icon-present text-primary\"></i>
            <h3>Free to Use</h3>
            <p class=\"text-muted\">As always, this theme is free to download and use for any purpose!</p>
        </div>
    </div>
    <div class=\"col-lg-6\">
        <div class=\"feature-item\">
            <i class=\"icon-lock-open text-primary\"></i>
            <h3>Open Source</h3>
            <p class=\"text-muted\">Since this theme is MIT licensed, you can use it commercially!</p>
        </div>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "/var/www/atelier/themes/vojtasvoboda-newage/partials/features/dummy.htm";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"row\">
    <div class=\"col-lg-6\">
        <div class=\"feature-item\">
            <i class=\"icon-screen-smartphone text-primary\"></i>
            <h3>Device Mockups</h3>
            <p class=\"text-muted\">Ready to use HTML/CSS device mockups, no Photoshop required!</p>
        </div>
    </div>
    <div class=\"col-lg-6\">
        <div class=\"feature-item\">
            <i class=\"icon-camera text-primary\"></i>
            <h3>Flexible Use</h3>
            <p class=\"text-muted\">Put an image, video, animation, or anything else in the screen!</p>
        </div>
    </div>
</div>
<div class=\"row\">
    <div class=\"col-lg-6\">
        <div class=\"feature-item\">
            <i class=\"icon-present text-primary\"></i>
            <h3>Free to Use</h3>
            <p class=\"text-muted\">As always, this theme is free to download and use for any purpose!</p>
        </div>
    </div>
    <div class=\"col-lg-6\">
        <div class=\"feature-item\">
            <i class=\"icon-lock-open text-primary\"></i>
            <h3>Open Source</h3>
            <p class=\"text-muted\">Since this theme is MIT licensed, you can use it commercially!</p>
        </div>
    </div>
</div>", "/var/www/atelier/themes/vojtasvoboda-newage/partials/features/dummy.htm", "");
    }
}
