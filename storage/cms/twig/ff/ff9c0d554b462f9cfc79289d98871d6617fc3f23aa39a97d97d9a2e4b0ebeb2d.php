<?php

/* /var/www/atelier/themes/jtherczeg-holidaycrown/pages/samples/intro.htm */
class __TwigTemplate_015c71b7210c9fd67f751a89b66707a6469c54226591039fe2bf5cb2118badee extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"container\">

    ";
        // line 3
        $context['__cms_content_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->contentFunction("intro/about.htm"        , $context['__cms_content_params']        );
        unset($context['__cms_content_params']);
        // line 4
        echo "
    <div class=\"spacer\">

    <div class=\"embed-responsive embed-responsive-16by9\"><iframe  class=\"embed-responsive-item\" src=\"//player.vimeo.com/video/55057393?title=0\" width=\"100%\" height=\"400\" frameborder=\"0\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>
    </div>

</div>";
    }

    public function getTemplateName()
    {
        return "/var/www/atelier/themes/jtherczeg-holidaycrown/pages/samples/intro.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  27 => 4,  23 => 3,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"container\">

    {% content 'intro/about.htm' %}

    <div class=\"spacer\">

    <div class=\"embed-responsive embed-responsive-16by9\"><iframe  class=\"embed-responsive-item\" src=\"//player.vimeo.com/video/55057393?title=0\" width=\"100%\" height=\"400\" frameborder=\"0\" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>
    </div>

</div>", "/var/www/atelier/themes/jtherczeg-holidaycrown/pages/samples/intro.htm", "");
    }
}
