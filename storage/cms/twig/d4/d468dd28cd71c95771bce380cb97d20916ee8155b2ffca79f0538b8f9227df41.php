<?php

/* /var/www/atelier/themes/vojtasvoboda-newage/partials/sections/download.htm */
class __TwigTemplate_cee44e75bd860759e12949eb4632fa8069729812c163014d1fc4951e456d50b9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<section class=\"download bg-primary text-center\" id=\"download\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-md-8 mx-auto\">
                <h2 class=\"section-heading\">
                    ";
        // line 6
        echo twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "theme", array()), "download_headline", array());
        echo "
                </h2>
                ";
        // line 8
        echo twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "theme", array()), "download_content", array());
        echo "
                <div class=\"badges\">
                    ";
        // line 10
        if (twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "theme", array()), "download_google_link", array())) {
            // line 11
            echo "                    <a class=\"badge-link\" href=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "theme", array()), "download_google_link", array()), "html", null, true);
            echo "\">
                        <img src=\"";
            // line 12
            echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/google-play-badge.svg");
            echo "\" alt=\"Google Play link\">
                    </a>
                    ";
        }
        // line 15
        echo "                    ";
        if (twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "theme", array()), "download_appstore_link", array())) {
            // line 16
            echo "                    <a class=\"badge-link\" href=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "theme", array()), "download_appstore_link", array()), "html", null, true);
            echo "\">
                        <img src=\"";
            // line 17
            echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/app-store-badge.svg");
            echo "\" alt=\"App Store link\">
                    </a>
                    ";
        }
        // line 20
        echo "                </div>
            </div>
        </div>
    </div>
</section>";
    }

    public function getTemplateName()
    {
        return "/var/www/atelier/themes/vojtasvoboda-newage/partials/sections/download.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 20,  57 => 17,  52 => 16,  49 => 15,  43 => 12,  38 => 11,  36 => 10,  31 => 8,  26 => 6,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<section class=\"download bg-primary text-center\" id=\"download\">
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-md-8 mx-auto\">
                <h2 class=\"section-heading\">
                    {{ this.theme.download_headline | raw }}
                </h2>
                {{ this.theme.download_content | raw }}
                <div class=\"badges\">
                    {% if this.theme.download_google_link %}
                    <a class=\"badge-link\" href=\"{{ this.theme.download_google_link }}\">
                        <img src=\"{{ 'assets/img/google-play-badge.svg' | theme }}\" alt=\"Google Play link\">
                    </a>
                    {% endif %}
                    {% if this.theme.download_appstore_link %}
                    <a class=\"badge-link\" href=\"{{ this.theme.download_appstore_link }}\">
                        <img src=\"{{ 'assets/img/app-store-badge.svg' | theme }}\" alt=\"App Store link\">
                    </a>
                    {% endif %}
                </div>
            </div>
        </div>
    </div>
</section>", "/var/www/atelier/themes/vojtasvoboda-newage/partials/sections/download.htm", "");
    }
}
