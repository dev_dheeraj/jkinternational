<?php

/* /var/www/atelier/themes/jtherczeg-holidaycrown/partials/bottom.htm */
class __TwigTemplate_daf064793a38232cb3f23c9b6e445bb20e46edd5f1bc35d24c167e132708567e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"container\">
            <div class=\"row\">
                <div class=\"col-sm-5\">
                    ";
        // line 4
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("bottom/about"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 5
        echo "                </div>
                 
                <div class=\"col-sm-3\">
                    ";
        // line 8
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("bottom/links"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 9
        echo "                </div>

                <div class=\"col-sm-4 subscribe\">
                    ";
        // line 12
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("bottom/socials"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 13
        echo "                </div>
            </div>
            <!--/.row--> 
        </div>
        <!--/.container-->";
    }

    public function getTemplateName()
    {
        return "/var/www/atelier/themes/jtherczeg-holidaycrown/partials/bottom.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 13,  42 => 12,  37 => 9,  33 => 8,  28 => 5,  24 => 4,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"container\">
            <div class=\"row\">
                <div class=\"col-sm-5\">
                    {% partial \"bottom/about\" %}
                </div>
                 
                <div class=\"col-sm-3\">
                    {% partial \"bottom/links\" %}
                </div>

                <div class=\"col-sm-4 subscribe\">
                    {% partial \"bottom/socials\" %}
                </div>
            </div>
            <!--/.row--> 
        </div>
        <!--/.container-->", "/var/www/atelier/themes/jtherczeg-holidaycrown/partials/bottom.htm", "");
    }
}
