<?php

/* /var/www/atelier/themes/vojtasvoboda-newage/partials/modules/nav-items.htm */
class __TwigTemplate_869d00b49b57457c151923fe5488d4147398b3b821f2a9c007c6be2dbac625ca extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if (twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "theme", array()), "download_enabled", array())) {
            // line 2
            echo "<li class=\"nav-item\">
    <a class=\"nav-link js-scroll-trigger\" href=\"#download\">
        Download
    </a>
</li>
";
        }
        // line 8
        if (twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "theme", array()), "features_enabled", array())) {
            // line 9
            echo "<li class=\"nav-item\">
    <a class=\"nav-link js-scroll-trigger\" href=\"#features\">
        Features
    </a>
</li>
";
        }
        // line 15
        if (twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "theme", array()), "contact_enabled", array())) {
            // line 16
            echo "<li class=\"nav-item\">
    <a class=\"nav-link js-scroll-trigger\" href=\"#contact\">
        Contact
    </a>
</li>
";
        }
    }

    public function getTemplateName()
    {
        return "/var/www/atelier/themes/vojtasvoboda-newage/partials/modules/nav-items.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  41 => 16,  39 => 15,  31 => 9,  29 => 8,  21 => 2,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if this.theme.download_enabled %}
<li class=\"nav-item\">
    <a class=\"nav-link js-scroll-trigger\" href=\"#download\">
        Download
    </a>
</li>
{% endif %}
{% if this.theme.features_enabled %}
<li class=\"nav-item\">
    <a class=\"nav-link js-scroll-trigger\" href=\"#features\">
        Features
    </a>
</li>
{% endif %}
{% if this.theme.contact_enabled %}
<li class=\"nav-item\">
    <a class=\"nav-link js-scroll-trigger\" href=\"#contact\">
        Contact
    </a>
</li>
{% endif %}", "/var/www/atelier/themes/vojtasvoboda-newage/partials/modules/nav-items.htm", "");
    }
}
