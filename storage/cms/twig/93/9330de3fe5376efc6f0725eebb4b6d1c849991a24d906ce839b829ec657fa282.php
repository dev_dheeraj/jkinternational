<?php

/* /var/www/atelier/themes/jtherczeg-holidaycrown/pages/home.htm */
class __TwigTemplate_b88bd43e09a703599b79eace3005658da38d8beaa1f7ed364cf2131360b9850f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- banner -->
<div class=\"banner\">
    ";
        // line 3
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("banner"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 4
        echo "</div>
<!-- banner-->

<!-- reservation-information -->
<div id=\"information\" class=\"spacer reserve-info \">
    ";
        // line 9
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("reserve"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 10
        echo "</div>
<!-- reservation-information -->

<!-- services -->
<div class=\"spacer services wowload fadeInUp\">
<div class=\"container\">
    ";
        // line 16
        $context['__cms_partial_params'] = [];
        echo $this->env->getExtension('Cms\Twig\Extension')->partialFunction("services"        , $context['__cms_partial_params']        , true        );
        unset($context['__cms_partial_params']);
        // line 17
        echo "</div>
</div>
<!-- services -->";
    }

    public function getTemplateName()
    {
        return "/var/www/atelier/themes/jtherczeg-holidaycrown/pages/home.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 17,  46 => 16,  38 => 10,  34 => 9,  27 => 4,  23 => 3,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!-- banner -->
<div class=\"banner\">
    {% partial \"banner\" %}
</div>
<!-- banner-->

<!-- reservation-information -->
<div id=\"information\" class=\"spacer reserve-info \">
    {% partial \"reserve\" %}
</div>
<!-- reservation-information -->

<!-- services -->
<div class=\"spacer services wowload fadeInUp\">
<div class=\"container\">
    {% partial \"services\" %}
</div>
</div>
<!-- services -->", "/var/www/atelier/themes/jtherczeg-holidaycrown/pages/home.htm", "");
    }
}
