<?php

/* /var/www/atelier/themes/vojtasvoboda-newage/partials/modules/footer.htm */
class __TwigTemplate_9960f68a38dda9af34681dda311054a6bf2c16a3b3e75000cff768541bc3aaf1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<footer>
    <div class=\"container\">
        <p>";
        // line 3
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "theme", array()), "footer", array()), "html", null, true);
        echo "</p>
        ";
        // line 4
        if (twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "theme", array()), "footer_menu", array())) {
            // line 5
            echo "        <ul class=\"list-inline\">
            <li class=\"list-inline-item\">
                <a href=\"#\">Privacy</a>
            </li>
            <li class=\"list-inline-item\">
                <a href=\"#\">Terms</a>
            </li>
            <li class=\"list-inline-item\">
                <a href=\"#\">FAQ</a>
            </li>
        </ul>
        ";
        }
        // line 17
        echo "    </div>
</footer>";
    }

    public function getTemplateName()
    {
        return "/var/www/atelier/themes/vojtasvoboda-newage/partials/modules/footer.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 17,  29 => 5,  27 => 4,  23 => 3,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<footer>
    <div class=\"container\">
        <p>{{ this.theme.footer }}</p>
        {% if this.theme.footer_menu %}
        <ul class=\"list-inline\">
            <li class=\"list-inline-item\">
                <a href=\"#\">Privacy</a>
            </li>
            <li class=\"list-inline-item\">
                <a href=\"#\">Terms</a>
            </li>
            <li class=\"list-inline-item\">
                <a href=\"#\">FAQ</a>
            </li>
        </ul>
        {% endif %}
    </div>
</footer>", "/var/www/atelier/themes/vojtasvoboda-newage/partials/modules/footer.htm", "");
    }
}
