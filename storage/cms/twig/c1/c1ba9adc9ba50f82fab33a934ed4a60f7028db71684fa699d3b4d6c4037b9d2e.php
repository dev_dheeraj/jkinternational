<?php

/* /var/www/atelier/themes/jtherczeg-holidaycrown/partials/bottom/links.htm */
class __TwigTemplate_4df6814a9c40fa15e0d9ae46ee35743c02e7d7220a316207853f1c247dbe0b99 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h4>Quick Links</h4>
                    <ul class=\"list-unstyled\">
                        <li><a href=\"";
        // line 3
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("samples/rooms");
        echo "\">Rooms &amp; Tariffs</a></li>        
                        <li><a href=\"";
        // line 4
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("samples/intro");
        echo "\">Introduction</a></li>
                        <li><a href=\"";
        // line 5
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("samples/gallery");
        echo "\">Gallery</a></li>
                        <li><a href=\"";
        // line 6
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("tour");
        echo "\">Tour Packages</a></li>
                        <li><a href=\"";
        // line 7
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("samples/contact");
        echo "\">Contact</a></li>
                    </ul>";
    }

    public function getTemplateName()
    {
        return "/var/www/atelier/themes/jtherczeg-holidaycrown/partials/bottom/links.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 7,  35 => 6,  31 => 5,  27 => 4,  23 => 3,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<h4>Quick Links</h4>
                    <ul class=\"list-unstyled\">
                        <li><a href=\"{{ 'samples/rooms'|page }}\">Rooms &amp; Tariffs</a></li>        
                        <li><a href=\"{{ 'samples/intro'|page }}\">Introduction</a></li>
                        <li><a href=\"{{ 'samples/gallery'|page }}\">Gallery</a></li>
                        <li><a href=\"{{ 'tour'|page }}\">Tour Packages</a></li>
                        <li><a href=\"{{ 'samples/contact'|page }}\">Contact</a></li>
                    </ul>", "/var/www/atelier/themes/jtherczeg-holidaycrown/partials/bottom/links.htm", "");
    }
}
