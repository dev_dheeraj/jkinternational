<?php

/* /var/www/atelier/themes/jtherczeg-holidaycrown/partials/services.htm */
class __TwigTemplate_2d708875b56baa063d98ee8289ee341b3565d984951fed9f5a95a071e23404aa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"row\">
        <div class=\"col-sm-4\">
            <!-- RoomCarousel -->
            <div id=\"RoomCarousel\" class=\"carousel slide\" data-ride=\"carousel\">
                <div class=\"carousel-inner\">
                <div class=\"item active\"><img src=\"";
        // line 6
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/photos/8.jpg");
        echo "\" class=\"img-responsive\" alt=\"slide\"></div>                
                <div class=\"item  height-full\"><img src=\"";
        // line 7
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/photos/9.jpg");
        echo "\"  class=\"img-responsive\" alt=\"slide\"></div>
                <div class=\"item  height-full\"><img src=\"";
        // line 8
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/photos/10.jpg");
        echo "\"  class=\"img-responsive\" alt=\"slide\"></div>
                </div>
                <!-- Controls -->
                <a class=\"left carousel-control\" href=\"#RoomCarousel\" role=\"button\" data-slide=\"prev\"><i class=\"fa fa-angle-left\"></i></a>
                <a class=\"right carousel-control\" href=\"#RoomCarousel\" role=\"button\" data-slide=\"next\"><i class=\"fa fa-angle-right\"></i></a>
            </div>
            <!-- RoomCarousel-->
            <div class=\"caption\">Rooms<a href=\"";
        // line 15
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("samples/rooms");
        echo "\" class=\"pull-right\"><i class=\"fa fa-edit\"></i></a></div>
        </div>


        <div class=\"col-sm-4\">
            <!-- RoomCarousel -->
            <div id=\"TourCarousel\" class=\"carousel slide\" data-ride=\"carousel\">
                <div class=\"carousel-inner\">
                <div class=\"item active\"><img src=\"";
        // line 23
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/photos/6.jpg");
        echo "\" class=\"img-responsive\" alt=\"slide\"></div>
                <div class=\"item  height-full\"><img src=\"";
        // line 24
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/photos/3.jpg");
        echo "\"  class=\"img-responsive\" alt=\"slide\"></div>
                <div class=\"item  height-full\"><img src=\"";
        // line 25
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/photos/4.jpg");
        echo "\"  class=\"img-responsive\" alt=\"slide\"></div>
                </div>
                <!-- Controls -->
                <a class=\"left carousel-control\" href=\"#TourCarousel\" role=\"button\" data-slide=\"prev\"><i class=\"fa fa-angle-left\"></i></a>
                <a class=\"right carousel-control\" href=\"#TourCarousel\" role=\"button\" data-slide=\"next\"><i class=\"fa fa-angle-right\"></i></a>
            </div>
            <!-- RoomCarousel-->
            <div class=\"caption\">Tour Packages<a href=\"";
        // line 32
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("samples/gallery");
        echo "\" class=\"pull-right\"><i class=\"fa fa-edit\"></i></a></div>
        </div>


        <div class=\"col-sm-4\">
            <!-- RoomCarousel -->
            <div id=\"FoodCarousel\" class=\"carousel slide\" data-ride=\"carousel\">
                <div class=\"carousel-inner\">
                <div class=\"item active\"><img src=\"";
        // line 40
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/photos/1.jpg");
        echo "\" class=\"img-responsive\" alt=\"slide\"></div>
                <div class=\"item  height-full\"><img src=\"";
        // line 41
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/photos/2.jpg");
        echo "\"  class=\"img-responsive\" alt=\"slide\"></div>
                <div class=\"item  height-full\"><img src=\"";
        // line 42
        echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/images/photos/5.jpg");
        echo "\"  class=\"img-responsive\" alt=\"slide\"></div>
                </div>
                <!-- Controls -->
                <a class=\"left carousel-control\" href=\"#FoodCarousel\" role=\"button\" data-slide=\"prev\"><i class=\"fa fa-angle-left\"></i></a>
                <a class=\"right carousel-control\" href=\"#FoodCarousel\" role=\"button\" data-slide=\"next\"><i class=\"fa fa-angle-right\"></i></a>
            </div>
            <!-- RoomCarousel-->
            <div class=\"caption\">Food and Drinks<a href=\"";
        // line 49
        echo $this->env->getExtension('Cms\Twig\Extension')->pageFilter("samples/gallery");
        echo "\" class=\"pull-right\"><i class=\"fa fa-edit\"></i></a></div>
        </div>
    </div>";
    }

    public function getTemplateName()
    {
        return "/var/www/atelier/themes/jtherczeg-holidaycrown/partials/services.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 49,  92 => 42,  88 => 41,  84 => 40,  73 => 32,  63 => 25,  59 => 24,  55 => 23,  44 => 15,  34 => 8,  30 => 7,  26 => 6,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div class=\"row\">
        <div class=\"col-sm-4\">
            <!-- RoomCarousel -->
            <div id=\"RoomCarousel\" class=\"carousel slide\" data-ride=\"carousel\">
                <div class=\"carousel-inner\">
                <div class=\"item active\"><img src=\"{{ 'assets/images/photos/8.jpg'|theme }}\" class=\"img-responsive\" alt=\"slide\"></div>                
                <div class=\"item  height-full\"><img src=\"{{ 'assets/images/photos/9.jpg'|theme }}\"  class=\"img-responsive\" alt=\"slide\"></div>
                <div class=\"item  height-full\"><img src=\"{{ 'assets/images/photos/10.jpg'|theme }}\"  class=\"img-responsive\" alt=\"slide\"></div>
                </div>
                <!-- Controls -->
                <a class=\"left carousel-control\" href=\"#RoomCarousel\" role=\"button\" data-slide=\"prev\"><i class=\"fa fa-angle-left\"></i></a>
                <a class=\"right carousel-control\" href=\"#RoomCarousel\" role=\"button\" data-slide=\"next\"><i class=\"fa fa-angle-right\"></i></a>
            </div>
            <!-- RoomCarousel-->
            <div class=\"caption\">Rooms<a href=\"{{ 'samples/rooms'|page }}\" class=\"pull-right\"><i class=\"fa fa-edit\"></i></a></div>
        </div>


        <div class=\"col-sm-4\">
            <!-- RoomCarousel -->
            <div id=\"TourCarousel\" class=\"carousel slide\" data-ride=\"carousel\">
                <div class=\"carousel-inner\">
                <div class=\"item active\"><img src=\"{{ 'assets/images/photos/6.jpg'|theme }}\" class=\"img-responsive\" alt=\"slide\"></div>
                <div class=\"item  height-full\"><img src=\"{{ 'assets/images/photos/3.jpg'|theme }}\"  class=\"img-responsive\" alt=\"slide\"></div>
                <div class=\"item  height-full\"><img src=\"{{ 'assets/images/photos/4.jpg'|theme }}\"  class=\"img-responsive\" alt=\"slide\"></div>
                </div>
                <!-- Controls -->
                <a class=\"left carousel-control\" href=\"#TourCarousel\" role=\"button\" data-slide=\"prev\"><i class=\"fa fa-angle-left\"></i></a>
                <a class=\"right carousel-control\" href=\"#TourCarousel\" role=\"button\" data-slide=\"next\"><i class=\"fa fa-angle-right\"></i></a>
            </div>
            <!-- RoomCarousel-->
            <div class=\"caption\">Tour Packages<a href=\"{{ 'samples/gallery'|page }}\" class=\"pull-right\"><i class=\"fa fa-edit\"></i></a></div>
        </div>


        <div class=\"col-sm-4\">
            <!-- RoomCarousel -->
            <div id=\"FoodCarousel\" class=\"carousel slide\" data-ride=\"carousel\">
                <div class=\"carousel-inner\">
                <div class=\"item active\"><img src=\"{{ 'assets/images/photos/1.jpg'|theme }}\" class=\"img-responsive\" alt=\"slide\"></div>
                <div class=\"item  height-full\"><img src=\"{{ 'assets/images/photos/2.jpg'|theme }}\"  class=\"img-responsive\" alt=\"slide\"></div>
                <div class=\"item  height-full\"><img src=\"{{ 'assets/images/photos/5.jpg'|theme }}\"  class=\"img-responsive\" alt=\"slide\"></div>
                </div>
                <!-- Controls -->
                <a class=\"left carousel-control\" href=\"#FoodCarousel\" role=\"button\" data-slide=\"prev\"><i class=\"fa fa-angle-left\"></i></a>
                <a class=\"right carousel-control\" href=\"#FoodCarousel\" role=\"button\" data-slide=\"next\"><i class=\"fa fa-angle-right\"></i></a>
            </div>
            <!-- RoomCarousel-->
            <div class=\"caption\">Food and Drinks<a href=\"{{ 'samples/gallery'|page }}\" class=\"pull-right\"><i class=\"fa fa-edit\"></i></a></div>
        </div>
    </div>", "/var/www/atelier/themes/jtherczeg-holidaycrown/partials/services.htm", "");
    }
}
