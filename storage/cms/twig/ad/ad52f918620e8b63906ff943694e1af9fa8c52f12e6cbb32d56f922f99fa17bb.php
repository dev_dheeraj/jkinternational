<?php

/* /var/www/atelier/themes/jtherczeg-holidaycrown/partials/footer.htm */
class __TwigTemplate_eb2420a91cf7daf83bc22decb1647a87de4627cb9778b9c93b7ed95e0be4bd9a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "Copyright &copy; ";
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "theme", array()), "site_name", array()), "html", null, true);
        echo ". All Rights Reserved. Powered by <a href=\"http://thebootstrapthemes.com\">thebootstrapthemes.com</a> &amp; <a href=\"http://octobercms.com\">OctoberCMS</a>";
    }

    public function getTemplateName()
    {
        return "/var/www/atelier/themes/jtherczeg-holidaycrown/partials/footer.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("Copyright &copy; {{ \"now\"|date(\"Y\") }} {{ this.theme.site_name }}. All Rights Reserved. Powered by <a href=\"http://thebootstrapthemes.com\">thebootstrapthemes.com</a> &amp; <a href=\"http://octobercms.com\">OctoberCMS</a>", "/var/www/atelier/themes/jtherczeg-holidaycrown/partials/footer.htm", "");
    }
}
