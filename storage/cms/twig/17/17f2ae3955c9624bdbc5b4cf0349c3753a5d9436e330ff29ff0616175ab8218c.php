<?php

/* /var/www/atelier/themes/vojtasvoboda-newage/partials/sections/intro.htm */
class __TwigTemplate_f80680730ac8d6a7964964b681f1c4f7240d1b2a503dc682f5bfacc2c87cc41b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<header class=\"masthead\">
    <div class=\"container h-100\">
        <div class=\"row h-100\">
            <div class=\"col-lg-7 my-auto\">
                <div class=\"header-content mx-auto\">
                    <h1 class=\"mb-5\">";
        // line 6
        echo twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "theme", array()), "intro_headline", array());
        echo "</h1>
                    ";
        // line 7
        if (twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "theme", array()), "intro_button_link", array())) {
            // line 8
            echo "                    <a href=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "theme", array()), "intro_button_link", array()), "html", null, true);
            echo "\" class=\"btn btn-outline btn-xl js-scroll-trigger\">
                        ";
            // line 9
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "theme", array()), "intro_button", array()), "html", null, true);
            echo "
                    </a>
                    ";
        }
        // line 12
        echo "                </div>
            </div>
            <div class=\"col-lg-5 my-auto\">
                <div class=\"device-container\">
                    <div class=\"device-mockup iphone6_plus portrait white\">
                        <div class=\"device\">
                            <div class=\"screen\">
                                <!-- Demo image for screen mockup, you can put an image here, some HTML, an animation, video, or anything else! -->
                                ";
        // line 20
        if (twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "theme", array()), "intro_img", array())) {
            // line 21
            echo "                                <img src=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "theme", array()), "intro_img", array()), "getThumb", array(0 => 621, 1 => 1104, 2 => "crop"), "method"), "html", null, true);
            echo "\" class=\"img-fluid\" alt=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "theme", array()), "site_title", array()), "html", null, true);
            echo "\" />
                                ";
        } else {
            // line 23
            echo "                                <img src=\"";
            echo $this->env->getExtension('Cms\Twig\Extension')->themeFilter("assets/img/demo-screen-1.jpg");
            echo "\" class=\"img-fluid\" alt=\"";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), ($context["this"] ?? null), "theme", array()), "site_title", array()), "html", null, true);
            echo "\" />
                                ";
        }
        // line 25
        echo "                            </div>
                            <!-- // You can hook the \"home button\" to some JavaScript events or just remove it
                            <div class=\"button\"></div>
                            -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>";
    }

    public function getTemplateName()
    {
        return "/var/www/atelier/themes/vojtasvoboda-newage/partials/sections/intro.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 25,  63 => 23,  55 => 21,  53 => 20,  43 => 12,  37 => 9,  32 => 8,  30 => 7,  26 => 6,  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<header class=\"masthead\">
    <div class=\"container h-100\">
        <div class=\"row h-100\">
            <div class=\"col-lg-7 my-auto\">
                <div class=\"header-content mx-auto\">
                    <h1 class=\"mb-5\">{{ this.theme.intro_headline | raw }}</h1>
                    {% if this.theme.intro_button_link %}
                    <a href=\"{{ this.theme.intro_button_link }}\" class=\"btn btn-outline btn-xl js-scroll-trigger\">
                        {{ this.theme.intro_button }}
                    </a>
                    {% endif %}
                </div>
            </div>
            <div class=\"col-lg-5 my-auto\">
                <div class=\"device-container\">
                    <div class=\"device-mockup iphone6_plus portrait white\">
                        <div class=\"device\">
                            <div class=\"screen\">
                                <!-- Demo image for screen mockup, you can put an image here, some HTML, an animation, video, or anything else! -->
                                {% if this.theme.intro_img %}
                                <img src=\"{{ this.theme.intro_img.getThumb(621, 1104, 'crop') }}\" class=\"img-fluid\" alt=\"{{ this.theme.site_title }}\" />
                                {% else %}
                                <img src=\"{{ 'assets/img/demo-screen-1.jpg' | theme }}\" class=\"img-fluid\" alt=\"{{ this.theme.site_title }}\" />
                                {% endif %}
                            </div>
                            <!-- // You can hook the \"home button\" to some JavaScript events or just remove it
                            <div class=\"button\"></div>
                            -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>", "/var/www/atelier/themes/vojtasvoboda-newage/partials/sections/intro.htm", "");
    }
}
