<?php

/* /var/www/atelier/themes/jtherczeg-holidaycrown/partials/bottom/socials.htm */
class __TwigTemplate_0365f04da8617b12005b3254d29dfab8876ef975f59c108a4143ba8fefedebb0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h4>Subscription</h4>
                    <div class=\"input-group\">
                    <input type=\"text\" class=\"form-control\" placeholder=\"Enter email id here\">
                    <span class=\"input-group-btn\">
                    <button class=\"btn btn-default\" type=\"button\">Get Notify</button>
                    </span>
                    </div>
                    <div class=\"social\">
                    <a href=\"#\"><i class=\"fa fa-facebook-square\" data-toggle=\"tooltip\" data-placement=\"top\" data-original-title=\"facebook\"></i></a>
                    <a href=\"#\"><i class=\"fa fa-instagram\"  data-toggle=\"tooltip\" data-placement=\"top\" data-original-title=\"instragram\"></i></a>
                    <a href=\"#\"><i class=\"fa fa-twitter-square\" data-toggle=\"tooltip\" data-placement=\"top\" data-original-title=\"twitter\"></i></a>
                    <a href=\"#\"><i class=\"fa fa-pinterest-square\" data-toggle=\"tooltip\" data-placement=\"top\" data-original-title=\"pinterest\"></i></a>
                    <a href=\"#\"><i class=\"fa fa-tumblr-square\" data-toggle=\"tooltip\" data-placement=\"top\" data-original-title=\"tumblr\"></i></a>
                    <a href=\"#\"><i class=\"fa fa-youtube-square\" data-toggle=\"tooltip\" data-placement=\"top\" data-original-title=\"youtube\"></i></a>
                    </div>";
    }

    public function getTemplateName()
    {
        return "/var/www/atelier/themes/jtherczeg-holidaycrown/partials/bottom/socials.htm";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<h4>Subscription</h4>
                    <div class=\"input-group\">
                    <input type=\"text\" class=\"form-control\" placeholder=\"Enter email id here\">
                    <span class=\"input-group-btn\">
                    <button class=\"btn btn-default\" type=\"button\">Get Notify</button>
                    </span>
                    </div>
                    <div class=\"social\">
                    <a href=\"#\"><i class=\"fa fa-facebook-square\" data-toggle=\"tooltip\" data-placement=\"top\" data-original-title=\"facebook\"></i></a>
                    <a href=\"#\"><i class=\"fa fa-instagram\"  data-toggle=\"tooltip\" data-placement=\"top\" data-original-title=\"instragram\"></i></a>
                    <a href=\"#\"><i class=\"fa fa-twitter-square\" data-toggle=\"tooltip\" data-placement=\"top\" data-original-title=\"twitter\"></i></a>
                    <a href=\"#\"><i class=\"fa fa-pinterest-square\" data-toggle=\"tooltip\" data-placement=\"top\" data-original-title=\"pinterest\"></i></a>
                    <a href=\"#\"><i class=\"fa fa-tumblr-square\" data-toggle=\"tooltip\" data-placement=\"top\" data-original-title=\"tumblr\"></i></a>
                    <a href=\"#\"><i class=\"fa fa-youtube-square\" data-toggle=\"tooltip\" data-placement=\"top\" data-original-title=\"youtube\"></i></a>
                    </div>", "/var/www/atelier/themes/jtherczeg-holidaycrown/partials/bottom/socials.htm", "");
    }
}
